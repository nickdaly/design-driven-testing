import unittest
from account import Account

class PopulateAccountFromXml(unittest.TestCase):
    def test_default(self):
        with open("tests/resources/NormalAccount.xml") as myxml:
            userXml = myxml.read()
    
        self.account.populateFrom(userXml)
        assertEquals("jsmith", account.username)
        assertEquals("John", account.firstname)
        assertEquals("Smith", account.lastname)
    
    def test_quotation_marks_in_each_element(self):
        with open("tests/resources/AccountWithQuotes.xml") as myxml:
            userXml = myxml.read()
    
        self.account.populateFrom(userXml)
        assertEquals("jsmith", account.username)
        assertEquals("John", account.firstname)
        assertEquals("Smith", account.lastname)
    
    def test_throw_error_if_expected_fields_are_missing(self):
        with open("tests/resources/NormalAccount.xml") as myxml:
            userXml = myxml.read()
    
        with self.assertRaises(XPathParseException):
            self.account.populateFrom(userXml)
    

    def setUp(self):
        self.account = Account()

if __name__ == "__main__":
    unittest.main()
