class AccountValidationException(Exception):
    pass

class Account(object):

    def __init__(self, username):
        self.username = username

    def validatePasswordSyntax(self, password):
        if (password.lower() == password) or (password.upper() == password):
            raise AccountValidationException(
                "Password must be a mixture of upper and lower case")
        if len(password) < 6:
            raise AccountValidationException(
                "Password must be at least 6 characters long")
        if not self._containsAtLeastOneDigit(password):
            raise AccountValidationException(
                "Password must contain at least 1 numeric digit.")
        return True

    def _containsAtLeastOneDigit(self, password):
        return True in [c.isdigit() for c in password]

    def populateFrom(self, userXml):
        helper = XPathHelper(userXml)
        self.username = helper.get("//username")
        self.firstname = helper.get("//firstname")
        self.lastname = helper.get("//lastname")

    def verifyPassword(self, password):
        pass
